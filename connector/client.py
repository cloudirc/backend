import sys
import datetime

from twisted.internet import defer, endpoints, protocol, reactor, task
from twisted.python import log
from twisted.words.protocols import irc

from twisted.web.client import Agent
from twisted.web.http_headers import Headers


class IRCConnector(irc.IRCClient):
    nickname = 'IRCConnectorBotThing'

    def __init__(self):
        self.deferred = defer.Deferred()

    def connectionLost(self, reason):
        self.deferred.errback(reason)

    def signedOn(self):
        for channel in self.factory.channels:
            self.join(channel)

    def privmsg(self, user, channel, message):
        nick, _, host = user.partition('!')
        handler.post(message.strip)

    def _sendMessage(self, msg, target, nick=None):
        if nick:
            msg = '%s, %s' % (nick, msg)
        self.msg(target, msg)

    def _showError(self, failure):
        return failure.getErrorMessage()


class HistoryHandler:
    def __init__(self):
        self.agent = Agent(reactor)
        self.db = []
        task.LoopingCall(self.send_to_web).start(5)

    def post(self, rest):
        self.db.append((datetime.datetime.now(), rest))

    def send_to_web(self):
        while self.db:
            message = self.db.pop()
            d = self.agent.request(
                'GET',
                'http://127.0.0.1:5001/'+message[1],
                Headers({'User-Agent': ['Twisted Web Client Example']}),
                None)
            d.addCallback(self.cbResponse)

    def cbResponse(self, ignored):
        print('Response received')


class IRCConnectorFactory(protocol.ReconnectingClientFactory):
    protocol = IRCConnector
    channels = ['#mytesting']

    def __init__(self, handler):
        self.handler = handler


def main(reactor, description, handler):
    endpoint = endpoints.clientFromString(reactor, description)
    factory = IRCConnectorFactory(handler)
    d = endpoint.connect(factory)
    d.addCallback(lambda protocol: protocol.deferred)
    return d


if __name__ == '__main__':
    log.startLogging(sys.stderr)
    handler = HistoryHandler()
    task.react(main, ['tcp:irc.freenode.net:6667', handler])
    reactor.run()
